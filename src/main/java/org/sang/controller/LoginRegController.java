package org.sang.controller;

import org.sang.bean.RespBean;
import org.sang.bean.User;
import org.sang.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vincent on 2021/04/15.
 */
@RestController
public class LoginRegController {

	@Autowired
	UserService userService;

	@RequestMapping("/login_error")
	public RespBean loginError() {
		return new RespBean("error", "登錄失敗!");
	}

	@RequestMapping("/login_success")
	public RespBean loginSuccess() {
		return new RespBean("success", "登錄成功!");
	}

	/**
	 * 如果自動跳轉到這個頁面，說明使用者未登錄，返回相應的提示即可
	 * <p>
	 * 如果要支持表單登錄，可以在這個方法中判斷請求的類型，進而決定返回JSON還是HTML頁面
	 *
	 * @return
	 */
	@RequestMapping("/login_page")
	public RespBean loginPage() {
		return new RespBean("error", "尚未登錄，請登錄!");
	}

	@PostMapping("/reg")
	public RespBean reg(User user) {
		int result = userService.reg(user);
		if (result == 0) {
			// 成功
			return new RespBean("success", "註冊成功!");
		} else if (result == 1) {
			return new RespBean("error", "用戶名重複，註冊失敗!");
		} else {
			// 失敗
			return new RespBean("error", "註冊失敗!");
		}
	}
}
