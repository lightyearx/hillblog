package org.sang.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by vincent on 2021/04/15.
 */
@Component
public class DataStatisticsComponent {
    @Autowired
    ArticleService articleService;

  //每天執行一次，統計PV
    @Scheduled(cron = "1 0 0 * * ?")
    public void pvStatisticsPerDay() {
        articleService.pvStatisticsPerDay();
    }
}
