package org.sang.service;

import org.sang.bean.Role;
import org.sang.bean.User;
import org.sang.mapper.RolesMapper;
import org.sang.mapper.UserMapper;
import org.sang.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * Created by vincent on 2021/04/15.
 */
@Service
@Transactional
public class UserService implements UserDetailsService {
	@Autowired
	UserMapper userMapper;
	@Autowired
	RolesMapper rolesMapper;
	@Autowired
	PasswordEncoder passwordEncoder;

	@Override
	public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
		User user = userMapper.loadUserByUsername(s);
		if (user == null) {
			// 避免返回null，這裡返回一個不含有任何值的User物件，在後期的密碼比對過程中一樣會驗證失敗
			return new User();
		}
		// 查詢使用者的角色資訊，並返回存入user中
		List<Role> roles = rolesMapper.getRolesByUid(user.getId());
		user.setRoles(roles);
		return user;
	}

	/**
	 * @param user
	 * @return 0表示成功 1表示用戶名重複 2表示失敗
	 */
	public int reg(User user) {
		User loadUserByUsername = userMapper.loadUserByUsername(user.getUsername());
		if (loadUserByUsername != null) {
			return 1;
		}
		// 插入使用者,插入之前先對密碼進行加密
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		user.setEnabled(true);// 用戶可用
		long result = userMapper.reg(user);
		// 配置使用者的角色，預設都是普通用戶
		String[] roles = new String[] { "2" };
		int i = rolesMapper.addRoles(roles, user.getId());
		boolean b = i == roles.length && result == 1;
		if (b) {
			return 0;
		} else {
			return 2;
		}
	}

	public int updateUserEmail(String email) {
		return userMapper.updateUserEmail(email, Util.getCurrentUser().getId());
	}

	public List<User> getUserByNickname(String nickname) {
		List<User> list = userMapper.getUserByNickname(nickname);
		return list;
	}

	public List<Role> getAllRole() {
		return userMapper.getAllRole();
	}

	public int updateUserEnabled(Boolean enabled, Long uid) {
		return userMapper.updateUserEnabled(enabled, uid);
	}

	public int deleteUserById(Long uid) {
		return userMapper.deleteUserById(uid);
	}

	public int updateUserRoles(Long[] rids, Long id) {
		int i = userMapper.deleteUserRolesByUid(id);
		return userMapper.setUserRoles(rids, id);
	}

	public User getUserById(Long id) {
		return userMapper.getUserById(id);
	}
}
